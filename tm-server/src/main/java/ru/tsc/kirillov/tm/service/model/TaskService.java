package ru.tsc.kirillov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import ru.tsc.kirillov.tm.api.repository.model.ITaskRepository;
import ru.tsc.kirillov.tm.api.service.IConnectionService;
import ru.tsc.kirillov.tm.api.service.model.ITaskService;
import ru.tsc.kirillov.tm.model.Task;
import ru.tsc.kirillov.tm.repository.model.TaskRepository;

import javax.persistence.EntityManager;

public class TaskService extends AbstractUserOwnedService<Task, ITaskRepository> implements ITaskService {

    public TaskService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    @Override
    protected ITaskRepository getRepository(@NotNull final EntityManager entityManager) {
        return new TaskRepository(entityManager);
    }

}
