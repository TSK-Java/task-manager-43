package ru.tsc.kirillov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kirillov.tm.dto.model.TaskDTO;

import java.util.List;

public interface ITaskRepositoryDTO extends IUserOwnedRepositoryDTO<TaskDTO> {

    @NotNull
    List<TaskDTO> findAllByProjectId(@Nullable String userId, @Nullable String projectId);

    void removeAllByProjectId(String userId, String projectId);

    void removeAllByProjectList(@Nullable String userId, @Nullable String[] projects);

}
