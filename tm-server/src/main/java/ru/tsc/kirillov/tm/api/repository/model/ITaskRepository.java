package ru.tsc.kirillov.tm.api.repository.model;

import ru.tsc.kirillov.tm.model.Task;

public interface ITaskRepository extends IUserOwnedRepository<Task> {
}
