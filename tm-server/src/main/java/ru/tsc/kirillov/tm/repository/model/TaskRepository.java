package ru.tsc.kirillov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.tsc.kirillov.tm.api.repository.model.ITaskRepository;
import ru.tsc.kirillov.tm.model.Task;

import javax.persistence.EntityManager;

public class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    public TaskRepository(@NotNull final EntityManager entityManager) {
        super(Task.class, entityManager);
    }

}
