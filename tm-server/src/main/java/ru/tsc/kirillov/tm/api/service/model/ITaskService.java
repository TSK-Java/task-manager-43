package ru.tsc.kirillov.tm.api.service.model;

import ru.tsc.kirillov.tm.model.Task;

public interface ITaskService extends IUserOwnedService<Task> {
}
