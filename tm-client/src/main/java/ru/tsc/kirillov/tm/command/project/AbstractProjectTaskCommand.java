package ru.tsc.kirillov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.tsc.kirillov.tm.api.endpoint.IProjectTaskEndpoint;
import ru.tsc.kirillov.tm.command.AbstractCommand;
import ru.tsc.kirillov.tm.enumerated.Role;

public abstract class AbstractProjectTaskCommand extends AbstractCommand {

    @NotNull
    public IProjectTaskEndpoint getProjectTaskEndpoint() {
        return getServiceLocator().getProjectTaskEndpoint();
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
